using UnityEngine;
using UnityEngine.Advertisements;

public class InitializeAdsScript : MonoBehaviour
{

    string gameId = "3979641";
    bool testMode = true;
    public string myPlacementId = "WelcomeToGameAd";
    void Start()
    {
        Advertisement.Initialize(gameId, testMode);
    }

    bool adShown = false;
    private void Update()
    {
        if (adShown == false && Advertisement.IsReady(myPlacementId))
        {
            adShown = true;
            Advertisement.Show(myPlacementId);
        }
    }
}
