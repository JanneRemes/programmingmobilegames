using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragControl : MonoBehaviour
{
    public Rigidbody selectedObject;
    public float force = 10;

    void Update()
    {
        // Select / unselect object
        if(Input.touchCount > 0)
        {
            Touch first = Input.touches[0];

            if(first.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(first.position);
                RaycastHit hit;
                if(Physics.Raycast(ray.origin, ray.direction, out hit, 100))
                {
                    selectedObject = hit.transform.gameObject.GetComponent<Rigidbody>();
                }
            }
            
            if(selectedObject != null)
            {
                Vector3 objectViewportCoordinates = Camera.main.WorldToViewportPoint(selectedObject.transform.position);
                objectViewportCoordinates.z = 0;

                Vector3 touchViewportCoordinates = Camera.main.ScreenToViewportPoint(first.position);
                touchViewportCoordinates.z = 0;

                Debug.Log("object viewport position " + objectViewportCoordinates + "\n" + "touch viewport position " + touchViewportCoordinates);
                Vector3 direction = touchViewportCoordinates - objectViewportCoordinates;
                selectedObject.AddForce(direction * force);
            }
        }
    }
}
